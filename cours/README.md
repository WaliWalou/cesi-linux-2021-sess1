# Mémos

* Mémos
  * [Commandes et fichiers essentiels du système](./basics.md)
  * [Réseau sous CentOS 7](./centos_network.md)
* [systemd](./systemd.md)
* [FHS](./fhs.md)
* [Partitionnement](./part.md)
* [SSH](./ssh_keys.md)
* [Introduction à la crypto](./crypto.md)