#!/bin/bash

echo "Argument reçu via l'appel du script : $1"
dir_to_backup=$1
destination_dir='/opt/backups'

if [[ $destination_dir == '/opt/backups' ]]
then
  echo "!!!! dest dir is /opt/backups"
else
  echo "!!!! dest dir is NOT /opt/backups"
fi

echo ${destination_dir}

do_something() {

  echo "test fonction"
  
  echo "l'argument reçu est : $1"
  echo "l'argument reçu en deuxième position est : $2"

}

do_something "argument n°1" "arg 2"
